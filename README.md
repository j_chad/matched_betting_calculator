# Matched Betting Calculator
Bet type: A - qualifying bet, B - stake NOT returned free bet, C - stake returned free bet.

#### Author
* James Chadwick, 2019

#### License
* Mozilla Public License 2.0
* Keep it free and always give credit where its due!
