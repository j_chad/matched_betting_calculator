#!/usr/bin/python


def calculator(calc_choice, back_stake, back_odds, lay_odds, lay_commision):  # , refund_val, refund_ret):
    print('\n====================')
    if calc_choice == 'a':
        # Qualifying bet and welcome bet formulas
        # a) Profit if back bet wins:
        # Profit = back stake * (back bet odds – 1) – lay stake * (lay bet odds – 1)
        # b) Profit if lay bet wins:
        # Profit = lay stake * (1 – commission) – back stake
        # c) To work out the ideal stake for even profit, no matter what the result:
        # Lay stake = back odds * back stake / (lay odds – commission)
        # d) Final profit for the qualifying bet and welcome bet stage:
        # Final profit = lay stake * (1 – commission) – back stake

        print('Qualifying bet')
        print('---------------')
        lay_stake = (back_stake*back_odds)/(lay_odds-(lay_commision/100))
        print('Lay stake: %.2f' % lay_stake)

        liability = lay_stake *(lay_odds-1)
        print('Liability: %.2f' % liability)

        profit = (lay_stake * (1-(lay_commision/100))) - back_stake
        print('Profit: %.2f' % profit)

        print('\n---------------')
        print('If Bookmaker (Back) bet wins')
        bookmaker = back_stake*(back_odds-1)
        print('Bookmaker: + %.2f' % bookmaker)

        print('Exchange: - %.2f' % liability)

        bookmaker_total = back_stake*(back_odds-1) - liability
        print('Total: %.2f' % bookmaker_total)

        print('\n---------------')
        print('If Exchange (Lay) bet wins')

        print('Bookmaker: - %.2f' % back_stake)

        exchange = lay_stake * (1-(lay_commision/100))
        print('Exchange: + %.2f' % exchange)

        exchange_total = exchange - back_stake
        print('Total: %.2f' % exchange_total)

    if calc_choice == 'b':
        # Free bet – stake NOT returned (SNR) formulas
        # a) Profit if the free bet SNR wins:
        # Profit = (back odds – 1) * free bet size – (lay odds – 1) * lay stake
        # b) Profit if the lay bet wins:
        # Profit = lay stake * (1 – commission)
        # c) To work out the ideal lay stake for the same profit, no matter what the result:
        # Lay stake for an even profit = (back odds – 1) / (lay odds – commission) * free bet size
        # d) Final profit for the free bet (SNR) stage:
        # Final profit = lay stake * (1 – commission)

        print('Free bet - stake NOT returned')
        print('---------------')
        lay_stake = ((back_odds-1) / (lay_odds - (lay_commision / 100))*back_stake)
        print('Lay stake: %.2f' % lay_stake)

        liability = lay_stake *(lay_odds-1)
        print('Liability: %.2f' % liability)

        profit = lay_stake * (1-(lay_commision/100))
        print('Profit: %.2f' % profit)

        print('\n---------------')
        print('If Bookmaker (Back) bet wins')
        bookmaker = back_stake * (back_odds - 1)
        print('Bookmaker: + %.2f' % bookmaker)

        print('Exchange: - %.2f' % liability)

        bookmaker_total = back_stake * (back_odds - 1) - liability
        print('Total: %.2f' % bookmaker_total)

        print('\n---------------')
        print('If Exchange (Lay) bet wins')

        print('Bookmaker: - %.2f' % 0)

        exchange_total = lay_stake * (1 - (lay_commision / 100))
        print('Exchange: + %.2f' % exchange_total)

    if calc_choice == 'c':
        # Free bet – stake returned (SR) formulas
        # a) Profit if free bet wins:
        # Profit = free bet value * back odds – lay stake * (lay odds – 1)
        # b) Profit if lay bet wins:
        # Profit = (1 – commission) * lay stake
        # c) To work out the ideal lay stake for even profit, no matter what the result:
        # Lay stake for an even profit = (back odds * free bet value) / (lay odds – commission)
        # d) Final profit for the free bet (SR) stage:
        # Final profit = (1 – commission) * lay stake

        print('Free bet - stake returned')
        print('---------------')
        lay_stake = (back_odds*back_stake)/(lay_odds-(lay_commision / 100))
        print('Lay stake: %.2f' % lay_stake)

        liability = lay_stake * (lay_odds - 1)
        print('Liability: %.2f' % liability)

        profit = lay_stake * (1 - (lay_commision / 100))
        print('Profit: %.2f' % profit)

        print('\n---------------')
        print('If Bookmaker (Back) bet wins')
        bookmaker = back_stake * (back_odds)
        print('Bookmaker: + %.2f' % bookmaker)

        print('Exchange: - %.2f' % liability)

        bookmaker_total = (back_stake * back_odds) - liability
        print('Total: %.2f' % bookmaker_total)

        print('\n---------------')
        print('If Exchange (Lay) bet wins')

        print('Bookmaker: - %.2f' % 0)

        exchange_total = lay_stake * (1 - (lay_commision / 100))
        print('Exchange: + %.2f' % exchange_total)

    # if calc_choice == 'd':
    #     # Guaranteed profit from refund bet
    #
    #     free_bet_val = refund_val * (refund_ret/100)
    #
    #     print(free_bet_val)


def main():
    calc_choice = input('Bet type: A - qualifying bet, B - stake NOT returned free bet, C - stake returned free bet, D - guaranteed profit refund bet.')
    back_stake = float(input('Back stake: '))
    back_odds = float(input('Back odds: '))
    lay_odds = float(input('Lay odds: '))
    lay_commision = float(input('Lay commission: '))

    # if calc_choice == 'd':
    #     refund_val = float(input('Refund value: '))
    #     refund_ret = float(input('Refund retention: '))
    # else:
    #     refund_val = 0
    #     refund_ret = 0

    calculator(calc_choice=calc_choice, back_stake=back_stake, back_odds=back_odds, lay_odds=lay_odds, lay_commision=lay_commision)  # , refund_val=refund_val, refund_ret=refund_ret)


if __name__ == "__main__":
    main()
